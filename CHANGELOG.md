# Changelog

All notable changes to this project will be documented in this file.

## Release 3.0.1

* fix release process #31

## Release 3.0.0

* set minimal required version for puppetlabs-stdlib 9.x #30 
* allow puppetlabs-stdlib < 10.0.0 #28
* set lower limit for puppet version_requirement to 7.0.0 #27
* use PDK 3.2.0 and Ruby 3.2.0 #26 

## Release 2.1.1

* Modify serve_config should restart cfssl serve process  #24

## Release 2.1.0

* add sysuser_manage parameter #17
* systemd unit for gencrl does not trigger on timer basis #20
* add crldir_manage parameter #16
* add crl_url for intermediate CA #19
* add crl_extension parameter #18

## Release 2.0.0

* move from liamstask/goose to pressly/goose #14
* Use Sensitive with parameter dbpassword #13
* Add Ubuntu22.04 as supported OS #12

## Release 1.1.0

* missing CRL for ROOT AC  #9
* fix README about CRL #10

## Release 1.0.0

Initial release
