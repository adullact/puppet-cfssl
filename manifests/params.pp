# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include cfssl::params
class cfssl::params {
  $binaries = ['cfssljson','cfssl-certinfo']
  $serve_config_json = 'serve-config.json'
  $db_config_json = 'db-config.json'
  $systemd_unitdir = '/etc/systemd/system'
  $systemd_unit_file = "${systemd_unitdir}/cfssl.service"
  $goose_downloadurl = 'https://github.com/pressly/goose/releases/download/v3.7.0/goose_linux_x86_64'
  $goose_checksumtype = 'sha256'
  $goose_checksum = 'd3b6b90af96e898c8a7e6c7b4fd45d04b994ca2f7fa4a6f66ef0fccfe31818df'
}
