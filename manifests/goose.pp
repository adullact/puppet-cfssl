# @summary Installs Goose, a database migration tool used by CFSSL
#
# Installs Goose, a database migration tool used by CFSSL
#
# @example
#   include cfssl::goose
#
# @api private
#
class cfssl::goose {
  assert_private()

  archive { "${cfssl::binpath}/goose" :
    ensure          => present,
    source          => $cfssl::params::goose_downloadurl,
    checksum_verify => true,
    checksum_type   => $cfssl::params::goose_checksumtype,
    checksum        => $cfssl::params::goose_checksum,
  }
  -> file { "${cfssl::binpath}/goose" :
    ensure => file,
    mode   => '0700',
    owner  => $cfssl::sysuser,
    group  => $cfssl::sysgroup,
  }
}
