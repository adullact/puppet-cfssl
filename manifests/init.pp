# @summary Install and configure CFSSL, serve process and CRL generation.
#
# Install and configure CFSSL, serve process and CRL generation.
#
# @example
#    class { 'cfssl':
#      rootca_manifest => {
#        cn      => 'MYEXEMPLE ROOT CA',
#        subject => {
#          'C' => 'FR',
#          'L' => 'MONTPELLIER',
#          'O' => 'MYEXEMPLE ORG',
#        },
#      },
#      intermediatesca => {
#        'MYEXEMPLE INTERMDIATE CA' => {
#          subject => {
#            'C' => 'FR',
#            'L' => 'MONTPELLIER',
#            'O' => 'MYEXEMPLE ORG',
#          },
#        },
#      },
#      serve_ca   => 'MYEXEMPLE INTERMDIATE CA',
#      crl_manage => true,
#    }
#
# @param downloadurl CFSSL download URL
# @param version Downloaded version of CFSSL binary
# @param downloadchecksum Checksum of CFSSL binary
# @param checksum_type Type of checksum used
# @param sysuser_manage To enable/disable the creation of sysuser and sysgroup. To permit manage users by external process.
# @param sysuser Operating system user account owner of CFSSL files
# @param sysgroup Operating system group owner of CFSSL files
# @param binding_ip IP adresse binded by CFSSL serve process.
# @param port The port used by CFSSL serve process.
# @param log_level The loglevel defined for CFSSL serve process.
# @param logdir The directory where log are written.
# @param dbname The name of database used by CFSSL.
# @param dbuser A Postgresql role used by CFSSL to connect the database.
# @param dbpassword A password of the dbuser.
# @param confdir The directory where configurations are written.
# @param binpath The path when CFSSL binaries are.
# @param rootca_manifest Manifest defining the root autority.
# @param intermediatesca Manifests defining intermediat autorities signed by root autority.
# @param serve_config The configuration of CFSSL serve process.
# @param crl_manage If true a CRL file is generated from the served autority.
# @param crldir A directory where the CRL file is written.
# @param crldir_manage Determines whether Puppet manages the `crldir` directory.
# @param crl_expiry A value, in seconds, after which the CRL should expire from the moment of the request
# @param crl_gentimer Systemd timer https://www.freedesktop.org/software/systemd/man/systemd.time.html
# @param crl_extension The filename extension suffix used form generated CRL.
# @param serve_ca The Certificate authority served with CFSSL serve
#
class cfssl (
  Stdlib::HTTPSUrl $downloadurl = 'https://github.com/cloudflare/cfssl/releases/download',
  String[1] $version = '1.6.3',
  String[1] $downloadchecksum = '16b42bfc592dc4d0ba1e51304f466cae7257edec13743384caf4106195ab6047',
  Enum['md5', 'sha1', 'sha2','sha256', 'sha384', 'sha512'] $checksum_type = 'sha256',
  Boolean $sysuser_manage = true,
  String[1] $sysuser = 'cfssl',
  String[1] $sysgroup = 'cfssl',
  Stdlib::IP::Address $binding_ip = '127.0.0.1',
  Stdlib::Port $port = 8080,
  Enum['0','1','2','3','4'] $log_level = '1',
  Stdlib::Absolutepath $logdir = '/var/log/cfssl',
  String[1] $dbname = 'db_cfssl',
  String[1] $dbuser = 'u_cfssl',
  Variant[String[1], Sensitive[String]] $dbpassword = Sensitive.new('changeme'),
  Stdlib::Absolutepath $confdir = '/etc/cfssl',
  Stdlib::Absolutepath $binpath = '/usr/local/bin',
  Hash $rootca_manifest = { cn => 'EXEMPLE ROOT CA', subject => { 'C' => 'FR', 'L' => 'MONTPELLIER', 'O' => 'EXEMPLE ORG', } },
  Hash $intermediatesca = {},
  Cfssl::Serveconfig $serve_config = { signing => { 'default' => { expiry => '1h', usages => ['client auth'] } } },
  Boolean $crl_manage = false,
  Stdlib::Absolutepath $crldir = '/var/cfssl',
  Boolean $crldir_manage = true,
  Integer $crl_expiry = 604800,
  String[1] $crl_gentimer = '*:00:00',
  String[1] $crl_extension  = 'pem',
  Optional[String[1]] $serve_ca = undef,
) inherits cfssl::params {
  include cfssl::goose
  include postgresql::server

  $_goose_cfssldbmigrate_path = "/home/${cfssl::sysuser}/goose-cfssldbmigrate"
  $_dbpassword_unsensitive = if $dbpassword =~ Sensitive[String] {
    $dbpassword.unwrap
  } else {
    $dbpassword
  }

  if $sysuser_manage {
    group { $sysgroup :
      ensure => present,
    }
    -> user { $sysuser :
      ensure     => present,
      managehome => true,
      shell      => '/bin/bash',
      gid        => $sysgroup,
      before     => [
        File["${binpath}/cfssl", $confdir, "${confdir}/ca", $logdir],
        File["${confdir}/${cfssl::params::db_config_json}"],
        File["${confdir}/${cfssl::params::serve_config_json}"],
        Vcsrepo[$_goose_cfssldbmigrate_path],
        Exec['goose pg up'],
        File["${cfssl::binpath}/goose"],
        Class['cfssl::ca::root'],
      ],
    }
  }

  archive { "${binpath}/cfssl" :
    ensure          => present,
    source          => "${downloadurl}/v${version}/cfssl_${version}_linux_amd64",
    checksum_verify => true,
    checksum_type   => $checksum_type,
    checksum        => $downloadchecksum,
  }
  -> file { "${binpath}/cfssl" :
    ensure => file,
    mode   => '0700',
    owner  => $sysuser,
    group  => $sysgroup,
  }

  $cfssl::params::binaries.each | String $_bin | {
    $_archiveurn = "v${version}/${_bin}_${version}_linux_amd64"

    archive { "${binpath}/${_bin}" :
      ensure          => present,
      source          => "${downloadurl}/${_archiveurn}",
      checksum_verify => false,
      subscribe       => Archive["${binpath}/cfssl"],
    }
    -> file { "${binpath}/${_bin}" :
      ensure  => file,
      mode    => '0700',
      owner   => $sysuser,
      group   => $sysgroup,
      require => File["${binpath}/cfssl"],
    }
  }

  file { [$confdir, "${confdir}/ca", $logdir]:
    ensure => directory,
    mode   => '0700',
    owner  => $sysuser,
    group  => $sysgroup,
  }

  if $crldir_manage {
    file { $crldir:
      ensure => directory,
      mode   => '0700',
      owner  => $sysuser,
      group  => $sysgroup,
    }
  }

  postgresql::server::db { $dbname:
    user     => $dbuser,
    password => postgresql::postgresql_password($dbuser, $dbpassword),
  }

  file { "${confdir}/${cfssl::params::db_config_json}":
    ensure  => file,
    mode    => '0600',
    owner   => $sysuser,
    group   => $sysgroup,
    content => epp('cfssl/db-config.json.epp'),
  }
  file { "${confdir}/${cfssl::params::serve_config_json}":
    ensure  => file,
    mode    => '0600',
    owner   => $sysuser,
    group   => $sysgroup,
    content => to_json_pretty($serve_config),
    notify  => Service['cfssl'],
  }

  vcsrepo { $_goose_cfssldbmigrate_path:
    ensure   => present,
    provider => git,
    source   => 'https://github.com/cloudflare/cfssl.git',
    revision => "v${version}",
    user     => $sysuser,
  }

  exec { 'goose pg up':
    command     => "${binpath}/goose postgres \"host=localhost user=${cfssl::dbuser} password='\$DBPASSWORD' dbname=${cfssl::dbname} sslmode=disable\" up",
    user        => $sysuser,
    environment => [
      "HOME=/home/${sysuser}/",
      "DBPASSWORD=${_dbpassword_unsensitive}",
    ],
    cwd         => "${_goose_cfssldbmigrate_path}/certdb/pg/migrations",
    onlyif      => "${binpath}/goose postgres \"host=localhost user=${cfssl::dbuser} password='\$DBPASSWORD' dbname=${cfssl::dbname} sslmode=disable\" status 2>&1 | grep -q 'Pending'",
    require     => [
      Vcsrepo[$_goose_cfssldbmigrate_path],
      Postgresql::Server::Db[$dbname],
      Class[cfssl::goose],
    ],
  }

  class { 'cfssl::ca::root':
    *       => $rootca_manifest,
    require => [
      File[$cfssl::confdir],
      File["${cfssl::confdir}/ca"],
      Archive["${binpath}/cfssl"],
      Archive["${binpath}/cfssljson"],
    ],
  }

  if $intermediatesca {
    class { 'cfssl::ca::intermediates':
      intermediates => $intermediatesca,
    }
  }

  if $serve_ca {
    file { $cfssl::params::systemd_unit_file:
      ensure  => file,
      mode    => '0644',
      owner   => 0,
      group   => 0,
      content => epp('cfssl/cfssl.service.epp'),
    }
    ~> service { 'cfssl':
      ensure    => 'running',
      enable    => true,
      require   => [
        Archive["${binpath}/cfssl"],
        Postgresql::Server::Db[$dbname],
        Exec['goose pg up'],
        File["${confdir}/${cfssl::params::serve_config_json}"],
        File["${confdir}/${cfssl::params::db_config_json}"],
        Class['cfssl::ca::root'],
      ],
      subscribe => Archive["${binpath}/cfssl"],
      provider  => 'systemd',
    }
  } else {
    service { 'cfssl':
      ensure    => 'stopped',
      enable    => false,
      require   => [
        Archive["${binpath}/cfssl"],
        Postgresql::Server::Db[$dbname],
        Exec['goose pg up'],
        File["${confdir}/${cfssl::params::serve_config_json}"],
        File["${confdir}/${cfssl::params::db_config_json}"],
        Class['cfssl::ca::root'],
      ],
      subscribe => Archive["${binpath}/cfssl"],
      provider  => 'systemd',
    }
  }

  if $cfssl::crl_manage {
    $_ca_list = [$rootca_manifest['cn'], $intermediatesca.keys, $serve_ca].flatten.unique
    class { 'cfssl::ca::gencrls':
      authorities => $_ca_list,
      require     => [
        Archive["${binpath}/cfssl"],
        Postgresql::Server::Db[$dbname],
        Exec['goose pg up'],
        Class['cfssl::ca::root'],
      ],
    }
  }
}
