# @summary Creates an intermediate authority signed by root authority
#
# Creates an intermediate authority signed by root authority
#
# @example
#   cfssl::ca::intermediate { 'MYEXEMPLE INTERMDIATE CA':
#     subject => {
#       'C'   => 'FR',
#       'L'   => 'MONTPELLIER',
#       'O'   => 'MYEXEMPLE ORG',
#     },
#   }
#
# @param subject Hash like Subject in X509 that identifies the entity associated
# @param expiry Duration of authority
# @param key Cryptographic algorithm used for creating key pairs.
# @param crl_url The CRL url of the Root CA that issued intermediate CA.
# @param ocsp_url The OCSP responder of the Root CA that issued intermediate CA.
#
define cfssl::ca::intermediate (
  Hash $subject = { 'C' => 'FR', 'L' => 'MONTPELLIER', 'O' => 'EXEMPLE ORG', },
  String[1] $expiry = '26280h',
  Cfssl::Ca::Key $key = { algo => 'rsa', size => 2048 },
  Optional[Stdlib::HTTPUrl] $crl_url = undef,
  Optional[Stdlib::HTTPUrl] $ocsp_url = undef,
) {
  require cfssl::ca::root

  $_rootca_name = regsubst($cfssl::ca::root::cn, '\s', '', 'G')
  $_rootca_cert = "${cfssl::confdir}/ca/${_rootca_name}.pem"
  $_rootca_privkey = "${cfssl::confdir}/ca/${_rootca_name}-key.pem"

  $_intermediatecn = $name
  $_intermediateca_name = regsubst($name, '\s', '', 'G')
  $_intermediateca_csr = {
    cn       => $_intermediatecn,
    names    => [$subject],
    key      => $key,
  }
  $_intermediateca_csr_json = to_json($_intermediateca_csr)

  $_root_to_intermediate_config = {
    signing => {
      'default' => {
        expiry        => $expiry,
        crl_url       => $crl_url,
        ocsp_url      => $ocsp_url,
        ca_constraint => {
          is_ca        => true,
          max_path_len => 1,
        },
        usages        => [
          'cert sign',
          'crl sign',
        ],
      },
    },
  }
  $_root_to_intermediate_config_file = "${cfssl::confdir}/ca/root-to-intermediate-${_intermediateca_name}_config.json"

  file { $_root_to_intermediate_config_file:
    ensure  => file,
    mode    => '0600',
    owner   => $cfssl::sysuser,
    group   => $cfssl::sysgroup,
    content => to_json_pretty($_root_to_intermediate_config),
  }
  -> exec { "genkey ${_intermediatecn}":
    path    => "/usr/bin:${cfssl::binpath}",
    command => "echo '${_intermediateca_csr_json}' | cfssl genkey - | cfssljson -bare ${cfssl::confdir}/ca/${_intermediateca_name}",
    creates => "${cfssl::confdir}/ca/${_intermediateca_name}-key.pem",
    user    => $cfssl::sysuser,
  }
  -> exec { "${cfssl::ca::root::cn} sign ${_intermediatecn} csr":
    path    => "/usr/bin:${cfssl::binpath}",
    command => "cfssl sign -ca ${_rootca_cert} -ca-key ${_rootca_privkey} -config ${_root_to_intermediate_config_file} ${cfssl::confdir}/ca/${_intermediateca_name}.csr | cfssljson -bare ${cfssl::confdir}/ca/${_intermediateca_name}",
    creates => "${cfssl::confdir}/ca/${_intermediateca_name}.pem",
    user    => $cfssl::sysuser,
  }
}
