# @summary Creates a service to generate CRL for a CA
#
#  Creates a service to generate CRL for a CA
#
# @example
#   cfssl::ca::gencrl { 'MYEXEMPLE INTERMDIATE CA': }
#
# @api private
#
# @param db_config_json Name of JSON config file for database usaed by CFSSL
# @param systemd_unitdir Directory where systemd units are created
define cfssl::ca::gencrl (
  String $db_config_json = $cfssl::params::db_config_json,
  Stdlib::Absolutepath $systemd_unitdir = $cfssl::params::systemd_unitdir,
) {
  assert_private()

  require cfssl::ca::root

  $_ca = regsubst($name, '\s', '', 'G')

  if $cfssl::crl_manage {
    ensure_packages('coreutils', { ensure => 'present' })

    file { "${systemd_unitdir}/cfssl-gencrl-${_ca}.service":
      ensure  => file,
      mode    => '0644',
      owner   => 0,
      group   => 0,
      content => epp('cfssl/cfssl-gencrl.service.epp', { 'ca' => $_ca }),
    }
    ~> service { "cfssl-gencrl-${_ca}.service":
      ensure   => 'stopped',
      enable   => true,
      provider => 'systemd',
    }

    file { "${systemd_unitdir}/cfssl-gencrl-${_ca}.timer":
      ensure  => file,
      mode    => '0644',
      owner   => 0,
      group   => 0,
      content => epp('cfssl/cfssl-gencrl.timer.epp', { 'ca' => $_ca }),
    }
    ~> service { "cfssl-gencrl-${_ca}.timer":
      ensure   => 'running',
      enable   => true,
      provider => 'systemd',
    }
  } else {
    service { ["cfssl-gencrl-${_ca}.service" ,"cfssl-gencrl-${_ca}.timer"]:
      ensure   => 'stopped',
      enable   => false,
      provider => 'systemd',
    }
    -> file { ["${systemd_unitdir}/cfssl-gencrl-${_ca}.service" ,"${systemd_unitdir}/cfssl-gencrl-${_ca}.timer"]:
      ensure  => absent,
    }
  }
}
