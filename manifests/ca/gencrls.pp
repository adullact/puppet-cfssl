# @summary  Creates services to generate CRL for a list of CA
#
# Creates services to generate CRL for a list of CA
#
# @example
#   class { 'cfssl::ca::gencrls':
#     authorities => [
#       'MYEXEMPLE ROOT CA',
#       'MYEXEMPLE INTERMDIATE CA',
#     ],
#   }
#
# @param authorities A list of authority CN's, already defined.
#
class cfssl::ca::gencrls (
  Array[String] $authorities = [],
) {
  $authorities.each | String[1] $_authority | {
    cfssl::ca::gencrl { $_authority:
    }
  }
}
