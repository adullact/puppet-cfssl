# @summary Init a selfsigned root authority
#
# Init a selfsigned root authority
#
# @example
#   class { 'cfssl::ca::root':
#     cn      => 'MYEXEMPLE ROOT CA',
#     subject => {
#       'C' => 'FR',
#       'L' => 'MONTPELLIER',
#       'O' => 'MYEXEMPLE ORG',
#     },
#   }
#
# @param subject Hash like Subject in X509 that identifies the entity associated
# @param cn Common name of authority
# @param expiry Duration of authority
# @param key Cryptographic algorithm used for creating key pairs.
#
class cfssl::ca::root (
  Hash $subject,
  String[1] $cn,
  String[1] $expiry = '43800h',
  Cfssl::Ca::Key $key = { algo => 'rsa', size => 2048 },
) {
  $_rootca_name = regsubst($cn, '\s', '', 'G')
  $_rootca_csr = {
    cn    => $cn,
    names => [$subject],
    ca    => { expiry => $expiry },
    key   => $key,
  }
  $_rootca_csr_json = to_json($_rootca_csr)

  exec { "initca ${cn}":
    path    => "/usr/bin:${cfssl::binpath}",
    command => "echo '${_rootca_csr_json}' | cfssl gencert -initca - | cfssljson -bare ${cfssl::confdir}/ca/${_rootca_name}",
    creates => "${cfssl::confdir}/ca/${_rootca_name}-key.pem",
    user    => $cfssl::sysuser,
  }
}
