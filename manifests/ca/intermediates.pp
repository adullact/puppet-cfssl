# @summary Creates `cfssl::ca::intermediate` defined types.
#
# Creates `cfssl::ca::intermediate` defined types.
#
# @example
#   class { 'cfssl::ca::intermediates':
#     'MYEXEMPLE INTERMDIATE CA' => {
#       subject => {
#         'C' => 'FR',
#         'L' => 'MONTPELLIER',
#         'O' => 'MYEXEMPLE ORG',
#       },
#       key     => {
#         algo => 'ecdsa',
#         size => 256,
#       },
#     },
#   }
#
# @param intermediates
#   A hash wherethe key is  the common name and the value represents a hash 
#   of `cfssl::ca::intermediate` defined type's parameters.
#
class cfssl::ca::intermediates (
  Hash $intermediates = {},
) {
  $intermediates.each | String[1] $_cn , Hash $_intermediate_manifest | {
    cfssl::ca::intermediate { $_cn:
      * => $_intermediate_manifest,
    }
  }
}
