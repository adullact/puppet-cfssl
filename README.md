# cfssl

This module install and configure [CFSSL](https://github.com/cloudflare/cfssl) to serve as a PKI.

## Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with cfssl](#setup)
    * [What cfssl affects](#what-cfssl-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with cfssl](#beginning-with-cfssl)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

With this module you can setup a self signed root certificat authority and
intermediates authorities signed by the root authority. Then via a API you 
can serve one of these authorities.

## Setup

### What cfssl affects

In addition to CFSSL, this module install Go, Goose and PostgreSQL.

### Beginning with cfssl

To setup your own self signed root authority, it is as simple as :
```
class { 'cfssl':
  rootca_manifest => {
    cn      => 'MYEXEMPLE ROOT CA',
    subject => {
      'C' => 'FR',
      'L' => 'MONTPELLIER',
      'O' => 'MYEXEMPLE ORG',
    },
  },
}
```

From here, you are able to procude certificates from `cfssl` command line with shell on the host.

## Usage

### With served self signed root certificate authority

In this example :
* self signed root ca is installed.
* this root ca is served via [HTTP API server](https://github.com/cloudflare/cfssl/tree/master/doc/api) for X509 certificates.
* a CRL is generated in `$cfssl::crldir` directory, updated periodicaly as defined by `$cfssl::crl_gentimer`.
* the CRL have a duration of validity defined by `cfssl::crl_expiry`

```
class { 'cfssl':
  rootca_manifest => {
    cn      => 'MYEXEMPLE ROOT CA',
    subject => {
      'C' => 'FR',
      'L' => 'MONTPELLIER',
      'O' => 'MYEXEMPLE ORG',
    },
  },
  serve_ca        => 'MYEXEMPLE ROOT CA',
  crl_manage      => true,
}

```

### With served intermediate certificate authority

In this example :
* a self signed root authority is installed.
* an intermediate authority signed by root authority is installed.
* this intermediate authority is served via [HTTP API server](https://github.com/cloudflare/cfssl/tree/master/doc/api) for X509 certificates.
* CRL are generated in `$cfssl::crldir` directory, updated periodicaly as defined by `$cfssl::crl_gentimer`.
* the CRL have a duration of validity defined by `cfssl::crl_expiry`

```
class { 'cfssl':
  rootca_manifest => {
    cn      => 'MYEXEMPLE ROOT CA',
    subject => {
      'C' => 'FR',
      'L' => 'MONTPELLIER',
      'O' => 'MYEXEMPLE ORG',
    },
  },
  intermediatesca => {
    'MYEXEMPLE INTERMDIATE CA' => {
      subject => {
        'C' => 'FR',
        'L' => 'MONTPELLIER',
        'O' => 'MYEXEMPLE ORG',
      },
    },
  },
  serve_ca   => 'MYEXEMPLE INTERMDIATE CA',
  crl_manage => true,
}
```

### Note about authkey

With CFSSL serve, it is possible to restrict acces to end points `sign` and `authsign`. In this case a [key](https://gitlab.adullact.net/adullact/puppet-cfssl/-/blob/main/REFERENCE.md#cfsslserveconfig) have to be
configured in [serve_config](https://gitlab.adullact.net/adullact/puppet-cfssl/-/blob/main/REFERENCE.md#serve_config) parameter.
It is possible to generated a key with this command : `hexdump -n 16 -e "4/4 \"%08X\" 1 \"\n\"" /dev/random`

## Reference

Details are in [REFERENCE.md](https://gitlab.adullact.net/adullact/puppet-cfssl/-/blob/main/REFERENCE.md) file.

## Limitations

Supported OSes are given in [metadata.json](https://gitlab.adullact.net/adullact/puppet-cfssl/-/blob/main/metadata.json) file.

## Development

Home at URL https://gitlab.adullact.net/adullact/puppet-cfssl

Issues and MR are welcome.

## Release Notes/Contributors/Etc.

Details in [CHANGELOG.md](https://gitlab.adullact.net/adullact/puppet-cfssl/-/blob/main/CHANGELOG.md).

```
Copyright (C) 2018 Association des Développeurs et Utilisateurs de Logiciels Libres
                     pour les Administrations et Colléctivités Territoriales.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/agpl.html>.

```

