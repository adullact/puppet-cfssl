require 'spec_helper_acceptance'

describe 'cfssl' do
  context 'with defaults and sysuser_manage to false' do
    pp = %(
      class { 'cfssl':
        sysuser_manage => false,
      }
    )

    it 'applies without error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe command('openssl x509 -in /etc/cfssl/ca/EXEMPLEROOTCA.pem -text -noout') do
      its(:stdout) { is_expected.to match %r{Certificate:} }
      its(:stdout) { is_expected.to match %r{Issuer: C = FR, L = MONTPELLIER, O = EXEMPLE ORG, CN = EXEMPLE ROOT CA} }
      its(:stdout) { is_expected.to match %r{Subject: C = FR, L = MONTPELLIER, O = EXEMPLE ORG, CN = EXEMPLE ROOT CA} }
      its(:stdout) { is_expected.to match %r{CA:TRUE} }
    end
  end

  context 'with defaults and sysuser_manage to true' do
    pp = %(
      include cfssl
    )

    it 'applies without error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe command('openssl x509 -in /etc/cfssl/ca/EXEMPLEROOTCA.pem -text -noout') do
      its(:stdout) { is_expected.to match %r{Certificate:} }
      its(:stdout) { is_expected.to match %r{Issuer: C = FR, L = MONTPELLIER, O = EXEMPLE ORG, CN = EXEMPLE ROOT CA} }
      its(:stdout) { is_expected.to match %r{Subject: C = FR, L = MONTPELLIER, O = EXEMPLE ORG, CN = EXEMPLE ROOT CA} }
      its(:stdout) { is_expected.to match %r{CA:TRUE} }
    end
  end

  context 'with served root ca' do
    pp = %(
      class { 'cfssl':
        rootca_manifest => {
          cn      => 'MYEXEMPLE ROOT CA',
          subject => {
            'C' => 'FR',
            'L' => 'MONTPELLIER',
            'O' => 'MYEXEMPLE ORG',
          },
        },
        serve_ca        => 'MYEXEMPLE ROOT CA',
        crl_manage      => true,
      }
    )

    it 'applies without error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
      shell('sleep 5') # gives some time for generating CRL
    end

    describe port(8080) do
      it { is_expected.to be_listening.on('127.0.0.1').with('tcp') }
    end

    describe command('curl -s -d "{}" -H "Content-Type: application/json" -X POST 127.0.0.1:8080/api/v1/cfssl/info') do
      its(:stdout) { is_expected.to match %r{BEGIN CERTIFICATE} }
    end

    describe command('openssl x509 -in /etc/cfssl/ca/MYEXEMPLEROOTCA.pem -text -noout') do
      its(:stdout) { is_expected.to match %r{Certificate:} }
      its(:stdout) { is_expected.to match %r{Issuer: C = FR, L = MONTPELLIER, O = MYEXEMPLE ORG, CN = MYEXEMPLE ROOT CA} }
      its(:stdout) { is_expected.to match %r{Subject: C = FR, L = MONTPELLIER, O = MYEXEMPLE ORG, CN = MYEXEMPLE ROOT CA} }
      its(:stdout) { is_expected.to match %r{CA:TRUE} }
    end

    describe command('openssl crl -in /var/cfssl/crl-MYEXEMPLEROOTCA.pem -text -noout') do
      its(:stdout) { is_expected.to match %r{Certificate Revocation List } }
      its(:stdout) { is_expected.to match %r{Issuer: C = FR, L = MONTPELLIER, O = MYEXEMPLE ORG, CN = MYEXEMPLE ROOT CA} }
      its(:stdout) { is_expected.to match %r{No Revoked Certificates} }
    end
  end

  context 'with served intermediate ca' do
    pp = %(
      class { 'cfssl':
        rootca_manifest => {
          cn      => 'MYEXEMPLE ROOT CA',
          subject => {
            'C' => 'FR',
            'L' => 'MONTPELLIER',
            'O' => 'MYEXEMPLE ORG',
          },
        },
        intermediatesca => {
          'MYEXEMPLE INTERMDIATE CA' => {
            subject => {
              'C' => 'FR',
              'L' => 'MONTPELLIER',
              'O' => 'MYEXEMPLE ORG',
            },
          },
          'MYEXEMPLE2 INTERMDIATE CA' => {
            subject => {
              'C' => 'FR',
              'L' => 'MONTPELLIER',
              'O' => 'MYEXEMPLE2 ORG',
            },
            crl_url => 'http://crl.exemple2.org/crl-MYEXEMPLE2INTERMDIATECA.crl',
          },
        },
        serve_ca   => 'MYEXEMPLE INTERMDIATE CA',
        crl_manage => true,
      }
    )

    it 'applies without error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
      shell('sleep 5') # gives some time for generating CRL
    end

    describe port(8080) do
      it { is_expected.to be_listening.on('127.0.0.1').with('tcp') }
    end

    describe command('curl -s -d "{}" -H "Content-Type: application/json" -X POST 127.0.0.1:8080/api/v1/cfssl/info') do
      its(:stdout) { is_expected.to match %r{BEGIN CERTIFICATE} }
    end

    describe command('openssl x509 -in /etc/cfssl/ca/MYEXEMPLEINTERMDIATECA.pem -text -noout') do
      its(:stdout) { is_expected.to match %r{Certificate:} }
      its(:stdout) { is_expected.to match %r{Issuer: C = FR, L = MONTPELLIER, O = MYEXEMPLE ORG, CN = MYEXEMPLE ROOT CA} }
      its(:stdout) { is_expected.to match %r{Subject: C = FR, L = MONTPELLIER, O = MYEXEMPLE ORG, CN = MYEXEMPLE INTERMDIATE CA} }
      its(:stdout) { is_expected.to match %r{CA:TRUE, pathlen:1} }
      its(:stdout) { is_expected.not_to match %r{X509v3 CRL Distribution Points:} }
    end
    describe command('openssl x509 -in /etc/cfssl/ca/MYEXEMPLE2INTERMDIATECA.pem -text -noout') do
      its(:stdout) { is_expected.to match %r{Certificate:} }
      its(:stdout) { is_expected.to match %r{Issuer: C = FR, L = MONTPELLIER, O = MYEXEMPLE ORG, CN = MYEXEMPLE ROOT CA} }
      its(:stdout) { is_expected.to match %r{Subject: C = FR, L = MONTPELLIER, O = MYEXEMPLE2 ORG, CN = MYEXEMPLE2 INTERMDIATE CA} }
      its(:stdout) { is_expected.to match %r{CA:TRUE, pathlen:1} }
      its(:stdout) { is_expected.to match %r{X509v3 CRL Distribution Points:} }
    end

    describe command('openssl crl -in /var/cfssl/crl-MYEXEMPLEROOTCA.pem -text -noout') do
      its(:stdout) { is_expected.to match %r{Certificate Revocation List } }
      its(:stdout) { is_expected.to match %r{Issuer: C = FR, L = MONTPELLIER, O = MYEXEMPLE ORG, CN = MYEXEMPLE ROOT CA} }
      its(:stdout) { is_expected.to match %r{No Revoked Certificates} }
    end
    describe command('openssl crl -in /var/cfssl/crl-MYEXEMPLEINTERMDIATECA.pem -text -noout') do
      its(:stdout) { is_expected.to match %r{Certificate Revocation List } }
      its(:stdout) { is_expected.to match %r{Issuer: C = FR, L = MONTPELLIER, O = MYEXEMPLE ORG, CN = MYEXEMPLE INTERMDIATE CA} }
      its(:stdout) { is_expected.to match %r{No Revoked Certificates} }
    end
  end
end
