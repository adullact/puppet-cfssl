# frozen_string_literal: true

require 'spec_helper'

describe 'cfssl' do
  let(:pre_condition) do
    'require postgresql::server'
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(service_provider: 'systemd') }

      it { is_expected.to compile }

      context 'with sysuser_manage to false' do
        let(:params) do
          {
            sysuser_manage: false,
          }
        end

        it { is_expected.to contain_file('/etc/cfssl/serve-config.json').that_notifies('Service[cfssl]') }
        it { is_expected.to compile }
      end
    end
  end
end
