# frozen_string_literal: true

require 'spec_helper'

describe 'cfssl::ca::intermediate' do
  let(:title) { 'namevar' }
  let(:pre_condition) do
    <<-PRECOND
    require postgresql::server
    include cfssl
    PRECOND
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(service_provider: 'systemd') }

      it { is_expected.to compile }
    end
  end
end
