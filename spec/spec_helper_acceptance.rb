require 'beaker-rspec'
require 'beaker-puppet'
require 'beaker/puppet_install_helper'
require 'beaker/module_install_helper'

run_puppet_install_helper
install_module_on(hosts)
install_module_dependencies_on(hosts)

RSpec.configure do |c|
  # Configure all nodes in nodeset
  c.before :suite do
    # vcsrepo expect git already installed
    # curl is used during tests to interact with CA
    pp_prepare_sut = %(
      package { ['git','curl']: ensure => present }
      group { 'cfssl' :
        ensure => present,
      }
      -> user { 'cfssl' :
        ensure     => present,
        managehome => true,
        shell      => '/bin/bash',
        gid        => 'cfssl',
      }
    )
    apply_manifest(pp_prepare_sut, catch_failures: true)
  end
end
