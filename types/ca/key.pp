# @summary Cryptographic algorithm used for creating key pairs.
type Cfssl::Ca::Key = Struct[{
    algo => Enum['rsa','ecdsa'],
    size => Integer,
}]
