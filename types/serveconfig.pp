# @summary Struct representing CFSSL serve configuration
type Cfssl::Serveconfig = Struct[{
    auth_keys => Optional[Hash[String,Cfssl::Authkey]],
    signing   => Struct[{
        'default' => Cfssl::Signing::Profile,
        profiles  => Optional[Hash[String,Cfssl::Signing::Profile]],
    }],
}]
