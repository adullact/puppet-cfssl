# @summary Struct describing a profile in CFSSL serve config file.
type Cfssl::Signing::Profile = Struct[{
    expiry         => String[1],
    usages         => Array[Cfssl::Usage],
    crl_url        => Optional[Stdlib::HTTPUrl],
    ocsp_url       => Optional[Stdlib::HTTPUrl],
    not_before     => Optional[String[1]],
    not_after      => Optional[String[1]],
    name_whitelist => Optional[String[1]],
    auth_key       => Optional[String[1]],
}]
