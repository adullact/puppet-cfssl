# @summary Struct representing authentication key used by CFSSL serve during sign requests
type Cfssl::Authkey = Struct[{
    type => Enum['standard'],
    key  => String[1],
}]
